package com.company;

import java.util.LinkedList;

public class SortedList {
    private LinkedList<Integer> sortedArray = null;
    private int inputInt;

    public SortedList(LinkedList<Integer> sortedArray) {
        this.sortedArray = sortedArray;
    }

    public SortedList() {
        sortedArray = new LinkedList<Integer>();
    }

    public void setInputInt(int inputInt) {
        this.inputInt = inputInt;
        sortedArray = sort(inputInt);

    }

    private LinkedList<Integer> sort(int input) {
        sortedArray.add(input);
        if (sortedArray.size() > 1) {
            int index = sortedArray.size() - 2;
            while (index > -1 && sortedArray.get(index) > input) {
                sortedArray.set(index + 1, sortedArray.get(index));
                sortedArray.set(index, input);
                index--;
            }

        }
        return sortedArray;

    }

    public LinkedList<Integer> getSortedArray() {
        return sortedArray;
    }

    public void printSortedArray() {
        System.out.println("");
        for (Integer printInt : sortedArray) {
            System.out.print(printInt + " ");
        }
    }
}
